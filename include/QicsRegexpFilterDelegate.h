/*********************************************************************
**
** Copyright (C) 2002-2014 Integrated Computer Solutions, Inc.
** All rights reserved.
**
** This file is part of the QicsTable software.
**
** See the top level README file for license terms under which this
** software can be used, distributed, or modified.
**
**********************************************************************/

#ifndef QICSREGEXPFILTERDELEGATE_H
#define QICSREGEXPFILTERDELEGATE_H

#include <QtGlobal>

#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
#include <QRegExp>
#else
#include <QRegularExpression>
#endif
#include "QicsAbstractFilterDelegate.h"


///////////////////////////////////////////////////////////////////////////////
// QicsRegexpFilterDelegate
///////////////////////////////////////////////////////////////////////////////

/*! \class QicsRegexpFilterDelegate QicsRegexpFilterDelegate.h
* \nosubgrouping
* \brief Class implements QRegExp-based data filter.
* \since 2.4
*/

////////////////////////////////////////////////////////////////////////

/*! \file */

////////////////////////////////////////////////////////////////////////

class QICS_EXPORT QicsRegexpFilterDelegate : public QicsAbstractFilterDelegate
{
    Q_OBJECT
public:
    /*! Constructor. Takes QRegExp as argument \a regexp.
    */
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QicsRegexpFilterDelegate(const QRegExp &regexp, QObject *parent = 0);
#else
    QicsRegexpFilterDelegate(const QRegularExpression &regexp, QObject *parent = 0);
#endif
    virtual ~QicsRegexpFilterDelegate();

    /*! Returns QRegExp.
    */
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    inline QRegExp regexp() const { return m_regexp; }
#else
    inline QRegularExpression regexp() const { return m_regexp; }
#endif

    /*! Sets QRegExp as argument \a regexp.
    */
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    inline void setRegexp(const QRegExp &regexp) {m_regexp = regexp;}
#else
    inline void setRegexp(const QRegularExpression &regexp) {m_regexp = regexp;}
#endif

    virtual bool match(const QString &cellContent, int row, int col);

private:
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QRegExp m_regexp;
#else
    QRegularExpression m_regexp;
#endif
};

#endif //QICSREGEXPFILTERDELEGATE_H


