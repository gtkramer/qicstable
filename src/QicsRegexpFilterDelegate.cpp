/*********************************************************************
**
** Copyright (C) 2002-2014 Integrated Computer Solutions, Inc.
** All rights reserved.
**
** This file is part of the QicsTable software.
**
** See the top level README file for license terms under which this
** software can be used, distributed, or modified.
**
**********************************************************************/

#include "QicsRegexpFilterDelegate.h"

#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
QicsRegexpFilterDelegate::QicsRegexpFilterDelegate(const QRegExp &regexp, QObject *parent)
#else
QicsRegexpFilterDelegate::QicsRegexpFilterDelegate(const QRegularExpression &regexp, QObject *parent)
#endif
    : QicsAbstractFilterDelegate(parent), m_regexp(regexp)
{
}

QicsRegexpFilterDelegate::~QicsRegexpFilterDelegate()
{
}

bool QicsRegexpFilterDelegate::match(const QString &cellContent, int row, int col)
{
    Q_UNUSED(row);
    Q_UNUSED(col);
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    return m_regexp.exactMatch(cellContent);
#else
    return m_regexp.match(cellContent).hasMatch();
#endif
}


